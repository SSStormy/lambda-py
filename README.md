# Lambda-py
Uploads:
* Screenshots (via scrot)
* Clipboard (via xclip, text only)
* Files

## Setup

You'll have to expose your lambda.sx API key in a file (.config/lambda/key) or in the LAMBDA_API_KEY shell variable.
After that, you're free to use the script.

## Usage
###### As of 1.0.0

```
Usage:
    lambdapy [-scn] (area | window | desktop)
    lambdapy [-scn] upload <file>
    lambdapy [-scn] clipboard
    lambdapy (-h | --help)

Commands:
    upload          Upload the specified file.
    clipboard       Upload the clipboard. (Text only)
    area            Take a screenshot of a certain area and upload it
    window          Take a screenshot of the current window and upload it
    desktop         Take a screenshot of the entire desktop and upload it

Options:
    -s --stdout     Print the URL to STDOUT
    -c --copy       Copy the URL to clipboard additionally to STDOUT
    -n --notif      Send a notify-send upon upload.
    -h --help       Show this help
```

#### License
MIT